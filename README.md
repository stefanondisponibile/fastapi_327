See reference: [#327](https://github.com/tiangolo/fastapi/issues/327)

`cd` into the cloned repo and add your `pwd` to your `PYTHONPATH`.

If you need *postgres*:
```bash
docker run --name tmp-postgres -p 5432:5432 -e POSTGRES_PASSWORD=foo -e POSTGRES_USER=foo postgres
```

Init db: `python app/db/init_db.py`

Start the application: `uvicorn app.main:app`

Try it out @ `http://localhost:8000/docs`