from sqlalchemy import Column, String, Integer
from sqlalchemy.dialects import postgresql
from app.db.base_class import Base


class Thing(Base):
    id = Column(Integer, primary_key=True, index=True)
    metadata_ = Column(postgresql.JSONB, default=[])
