from typing import List

from pydantic import BaseModel

from .metadata import Metadata


# Shared properties
class ThingBase(BaseModel):
    id: int = None
    metadata_: List[Metadata]

    class Config:
        orm_mode = True


# Properties to receive on thing creation
class ThingCreate(ThingBase):
    pass


# Properties to receive on item update
class ThingUpdate(ThingBase):
    pass


# Properties shared by models stored in DB
class ThingInDBBase(ThingBase):
    class Config:
        allow_population_by_alias = True


# Properties to return to the client
class Thing(ThingInDBBase):
    class Config:
        fields = {"metadata_": {"alias": "metadata"}}


# Other properties stored in DB
class ThingInDB(ThingInDBBase):
    pass
