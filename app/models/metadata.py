from pydantic import BaseModel

class Metadata(BaseModel):
    title: str = None