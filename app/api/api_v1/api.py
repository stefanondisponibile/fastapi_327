from fastapi import APIRouter
from app.api.api_v1.endpoints import things

router = APIRouter()
router.include_router(things.router, prefix="/things")
