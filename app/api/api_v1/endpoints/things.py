from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.models.thing import Thing, ThingCreate

router = APIRouter()

@router.get("/", response_model=List[Thing])
def read_things(
    *,
    db: Session = Depends(get_db),
    skip: int  = 0,
    limit: int = 100,
):
    """
    Retrieve things.
    """
    things = crud.thing.get_multi(db_session=db, skip=skip, limit=limit)
    return things

@router.post("/", response_model=Thing)
def create_thing(
    *,
    db: Session = Depends(get_db),
    thing_in: ThingCreate,
):
    """
    Create new thing.
    """
    thing = crud.thing.create(db_session=db, thing_in=thing_in)
    return thing
