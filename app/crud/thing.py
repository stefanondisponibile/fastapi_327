from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.thing import Thing
from app.models.thing import ThingCreate


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[Thing]]:
    things = db_session.query(Thing).offset(skip).limit(limit).all()
    return things


def get(db_session: Session, *, id: int) -> Optional[Thing]:
    return db_session.query(Thing).filter(Thing.id == id).first()


def create(db_session: Session, *, thing_in: ThingCreate) -> Thing:
    thing_in_data = jsonable_encoder(thing_in, skip_defaults=True)
    thing = Thing(**thing_in_data)
    db_session.add(thing)
    db_session.commit()
    db_session.refresh(thing)
    return thing
