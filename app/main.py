from fastapi import FastAPI
from starlette.requests import Request
from app.db.session import Session
from app.api.api_v1 import api

app = FastAPI()
app.include_router(api.router, prefix="/api/v1")
@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    request.state.db = Session()
    response = await call_next(request)
    request.state.db.close()
    return response
